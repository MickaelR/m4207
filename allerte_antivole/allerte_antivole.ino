#include <Wire.h>
#include <LiquidCrystal.h>
#include "rgb_lcd.h"
#include <Wire.h>
#include "MMA7660.h"
MMA7660 accelemeter;

rgb_lcd lcd;


// set LED pin numbers
int led_vert = 7;
int led_rouge = 3;

// set PushButton pin number
const int buttonPin = 2;
int buttonState = LOW;
int previous=0;
long times=0;


//-----------------------------------------------------------//
String securite = "Securite Active" ;
String vulnerable = "Pas de Securite" ;  //liste des message
String desactivation = "Desactivation...";
//---------------------------------------------------------//

int X = 0;

void setup() {
  accelemeter.init();
  Serial.begin(9600);
  lcd.begin(16, 2); //parametrage du lcd
  pinMode(led_vert, OUTPUT);
  pinMode(led_rouge, OUTPUT);
  pinMode(6,OUTPUT);
  digitalWrite(6,LOW);
}


void loop() {
  // put your main code here, to run repeatedly:
  delay(10);
//accelerometre------------------------------------------------------------------//
  int8_t x;
  int8_t y;
  int8_t z;
  float ax,ay,az;
  accelemeter.getXYZ(&x,&y,&z);
    
  //Serial.print("x = ");
    //Serial.println(x); 
    //Serial.print("y = ");
    //Serial.println(y);   
    //Serial.print("z = ");
    //Serial.println(z);
    
   accelemeter.getAcceleration(&ax,&ay,&az);
    //Serial.println("accleration of X/Y/Z: ");
    //Serial.print(ax);
    //Serial.println(" g");
    //Serial.print(ay);
    //Serial.println(" g");
    //Serial.print(az);
    //Serial.println(" g");
    //Serial.println("*************");
//Fin accelrometre--------------------------------------------------------------------//

  // simple ou double appui //
  buttonState = digitalRead(buttonPin);

  //--------------------------------------------------------//
  if (X == 0) //message "pas de sécurité par défaut"
  {
    digitalWrite(led_vert, LOW);
    digitalWrite(led_rouge, HIGH);
    lcd.setRGB(200, 0, 0);
    lcd.clear();
    lcd.setCursor(1, 0);
    lcd.print(vulnerable);
    delay(20);
    if (buttonState == 1) {
      Serial.println("OK");
      //Serial.println(buttonState);
      X=1;
    }
  }

  //--------------------------------------------------------------//
  delay(10);
  if (X == 1) {
    lcd.setRGB(0, 200, 0);  // Affichage du mode sécurité
    lcd.clear();
    lcd.setCursor(1, 0);
    lcd.print(securite);
    digitalWrite(led_vert, HIGH); //Allume la led
    digitalWrite(led_rouge, LOW);
    //Serial.println(buttonState);
    accelemeter.getXYZ(&x,&y,&z);
    if (abs(x)>15 || abs(y)>15){
      Serial.println(abs(x));
      Serial.println(abs(y));
      digitalWrite(6,HIGH);
      delay(200);
      digitalWrite(6,LOW);
    }
    delay(100);
    if(previous ==1 && millis() > (times+200) ){
      previous =0;
    }
      if(digitalRead(buttonPin) ==1 && previous == 1){
        previous=0;
        X=2;
      }
      else if(digitalRead(buttonPin) ==1){
        previous=1;
        times=millis();
        delay(20);
      }
  }

  if (X==2) {
    delay(500);
    lcd.setRGB(100, 100, 300);   // désactivation du mode sécurité
    digitalWrite(led_vert, HIGH); //Allume la led
    digitalWrite(led_rouge, HIGH);
    lcd.clear();
    lcd.print(desactivation);
    //Serial.println(buttonState);
    delay(3000); //delai de sécurité
    X=0;
    delay(1000);
  }

}
