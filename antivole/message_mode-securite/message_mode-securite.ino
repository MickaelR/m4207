#include <Wire.h>
#include <LiquidCrystal.h>
#include "rgb_lcd.h"

rgb_lcd lcd;

int led_vert = 7;
int led_rouge = 3;

String securite = "Securite Active" ;
String vulnerable = "Pas de Securite" ;  //liste des message
String desactivation = "Desactivation...";

int X = 0;
void setup() {
  lcd.begin(16,2); //parametrage du lcd
  pinMode(led_vert, OUTPUT);
  pinMode(led_rouge, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  delay(10);

  //--------------------------------------------------------//
  while(X==0)   //message "pas de sécurité par défaut"
  {
      digitalWrite(led_vert,LOW);
      digitalWrite(led_rouge,HIGH);
      lcd.setRGB(200, 0, 0);
      lcd.clear();
      lcd.setCursor(1, 0);
      lcd.print(vulnerable);
      delay(20);
      if(analogRead(A0)>100){
      X=1;
      }
    }

 //--------------------------------------------------------------//   
   delay(10);
     if(analogRead(A0)>0){ // passage en mode sécurité
      X=1;
     }
   delay(10);
//---------------------------------------------------------------//

   while(X==1){
     
      lcd.setRGB(0, 200, 0);  // Affichage du mode sécurité
      lcd.clear();
      lcd.setCursor(1, 0);
      lcd.print(securite);
      digitalWrite(led_vert,HIGH); //Allume la led
      digitalWrite(led_rouge,LOW);
      delay(10);
      
      while(analogRead(A0)>100){
        delay(500);
        if(analogRead(A0)>100){
          lcd.setRGB(100, 100, 300);   // désactivation du mode sécurité
          digitalWrite(led_vert,HIGH); //Allume la led
          digitalWrite(led_rouge,HIGH);
          lcd.clear();
          lcd.print(desactivation);
          delay(3000); //delai de sécurité
          X=0;
        }
        delay(1000); 
        }
   }
 }
