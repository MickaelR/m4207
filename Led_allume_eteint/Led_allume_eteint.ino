void setup() {
 // put your setup code here, to run once:
 pinMode(13,OUTPUT);
 Serial.begin(9600); //lance le moniteur
}
void loop() {
 // put your main code here, to run repeatedly:
 digitalWrite(13,HIGH); // Allume la led
 Serial.println(" La LED s’allume"); // affiche le message
 delay(100); // delay d'attente
 digitalWrite(13,LOW); //éteint la led
 Serial.println("La LED s’éteint"); //affiche le message
 delay(100); // delay d'attente

}
