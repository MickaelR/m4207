int compteur=0;
void setup() {
 // put your setup code here, to run once:
 Serial.begin(9600); //lance le moniteur
 Serial.println("C'est Parti!"); // affiche le message
}
void loop() {
  // put your main code here, to run repeatedly:
  Serial.print("la valeur du compteur est : "); // affichage sans passer à la ligne
  Serial.println(compteur); // affiche la valeur du compteur
  delay(250); // delay d'attente
  compteur += 1; // incrémente le compteur
}
